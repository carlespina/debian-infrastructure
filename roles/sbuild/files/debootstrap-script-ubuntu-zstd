# extend gutsy
. /usr/share/debootstrap/scripts/gutsy

# Debian's dpkg cannot deal with zstd, so force ar instead.
EXTRACTOR_OVERRIDE=ar

# extend the ar extractor to support zstd
extract_ar_deb_data () {
	local pkg tarball
	pkg="$1"
	tarball="$(ar -t "$pkg" | grep "^data.tar")"

	case "$tarball" in
		data.tar.gz)  cat_cmd=zcat ;;
		data.tar.bz2) cat_cmd=bzcat ;;
		data.tar.xz)  cat_cmd=xzcat ;;
		data.tar.zst) cat_cmd=zstdcat ;;
		data.tar)     cat_cmd=cat ;;
		*) error 1 UNKNOWNDATACOMP "Unknown compression type for %s in %s" "$tarball" "$pkg" ;;
	esac

	if in_path "$cat_cmd"; then
		ar -p "$pkg" "$tarball" | "$cat_cmd" | tar $EXTRACT_DEB_TAR_OPTIONS -xf -
	else
		error 1 UNPACKCMDUNVL "Extracting %s requires the %s command, which is not available" "$pkg" "$cat_cmd"
	fi
}
