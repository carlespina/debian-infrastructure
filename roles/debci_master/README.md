debci\_master
=============

Install and configure debci-collector with a local postgresql database, a
local rabbitmq-server and serve the web interface via apache2 and passenger.

Role Variables
--------------

`debci_arch_list` is a list of architectures that are being tested.
It should contain all architectures that are configured on any connected worker.

`debci_server_name` is the public DNS name of the webserver.

`debci_suite_list` is a list of suite names that are being tested.
It should contain all suites that are configured on any connected worker.

License
-------

MIT

Author Information
------------------
Sébastien Delafond <sdelafond@gmail.com>
Helmut Grohne <helmut@freexian.com>
