Rebuildd
========

Install and configure rebuildd, a debian package build daemon.

Role Variables
--------------

Multiple variables are required to get something useful. Here's a
minimal example:

    rebuildd_dists: bullseye
    rebuildd_architectures: amd64
    rebuildd_log_email: joe@example.com
    rebuildd_dput_rules:
      - name: default
        method: scp
        fqdn: target-host.example.com
        incoming: /srv/incoming
        login: rhertzog
    rebuildd_ssh_keys_to_add_jobs:
      - comment: SSH key of the repository master
        ssh_key: ssh-rsa AAAA...==
  
Other variables that you might find useful:

    # Override the URL where source packages are fetched
    rebuildd_fetch_mirror_url: http://deb.debian.org/debian/pool
    # Override the dput profile used by default
    rebuildd_dput_default_profile: default

The default helper scripts have hooks so that you can inject custom code
with `rebuildd_fetch_code_to_identify_mirror`,
`rebuildd_build_code_to_set_cmdline` and
`rebuildd_upload_code_to_set_dput_profile`.

Alternatively, you can provide your own templates replacing the 3 helper
scripts entirely:

    rebuildd_fetch_script: my-fetch.j2
    rebuildd_build_script: my-build.j2
    rebuildd_upload_script: my-upload.j2

Example Playbook
----------------

    - name: Configure rebuildd
      hosts: buildd
      roles:
        - role: rebuildd

License
-------

MIT

Author Information
------------------

Raphaël Hertzog <raphael@freexian.com>
Sébastien Delafond <seb@freexian.com>
