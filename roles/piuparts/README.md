# piuparts

Install piuparts tailored for a debusine-worker: invoked as non-root,
mass usage with untrusted input.

The main security issue is that a malicious package could attempt to
break from piupart's chroot and take control of the host, e.g. through
Debian package installation scripts. piuparts has been requiring root
privileges, although work is underway to alleviate this requirement.

The current implementation of this role conservatelively mimics
piuparts.debian.org using a sudo-based setup, along with cron jobs to
handle stale runs and left-over files.

This setup is not particularly secure, though note that
piuparts.debian.org runs dedicated piuparts nodes with no sensitive
data. Consequently this role is currently meant for test debusine
servers or dedicated nodes.

piuparts can generate Debian chroots on-demand through debootstrap;
for performance it is possible to reuse schroots created by the sbuild
role using the '--schroot' option.

This is subject to change within the next few months (as of 2023-11)
depending on debusine's direction, see in particular:
https://salsa.debian.org/freexian-team/debusine/-/issues/189
https://salsa.debian.org/freexian-team/debusine/-/issues/166

## License

MIT

## Author Information

Sylvain Beucler <beuc@beuc.net>
